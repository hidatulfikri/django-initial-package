# Django Initial Package

This is a package containing initial files essential for a Django project.

# Project Title

[![pipeline status](https://gitlab.com/<group-name>/<project-name>/badges/master/pipeline.svg)](https://gitlab.com/<group-name>/<project-name>/commits/master) 
[![coverage report](https://gitlab.com/<group-name>/<project-name>/badges/master/coverage.svg)](https://gitlab.com/<group-name>/<project-name>/commits/master)

<Overview>

## Access

* [Repository Link](https://gitlab.com/<group-name>/<project-name>)
* [Heroku Link](http://<app-name>.herokuapp.com)




## Author(s)

* **Hidayatul Fikri** - [hidatulfikri](https://gitlab.com/hidatulfikri)


## Acknowledgement

* Acknowledgements